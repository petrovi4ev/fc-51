/*
An example.
*/

package main

import (
	"fmt"
	"os"
	"time"

	"github.com/stianeikeland/go-rpio"
)

var (
	// Use mcu pin 7, corresponds to GPIO 7 on the pi
	pin = rpio.Pin(4)
)

func main() {
	// Open and map memory to access gpio, check for errors
	if err := rpio.Open(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// Unmap gpio memory when done
	defer rpio.Close()

	pin.Input()
	pin.PullUp()
	pin.Detect(rpio.FallEdge) // enable falling edge event detection
        defer pin.Detect(rpio.NoEdge) // disable edge event detection

	for {
		if pin.EdgeDetected() { // check if event occured
			fmt.Println("fc51 detected motion!")
			time.Sleep(time.Second / 2)
		}
	}
}

